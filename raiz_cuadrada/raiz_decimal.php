<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Convertir radicando a cadena de caracteres
$radicando = '120';
$raiz_aprox = '0';
$resto = '';
$auxiliar = '';
$cant_decimales = 3;

// Obtener pares de números del radicando
if (strlen($radicando) % 2 == 0) {
    $pares = str_split($radicando, 2);
} else {
    $pares = array_merge(array($radicando[0]), str_split(substr($radicando, 1), 2));
}

# Agregar coma
array_push($pares,".");

# Agregar tantos pares de ceros como decimales deseados
for ($i = 1; $i <= $cant_decimales; $i++) {
	array_push($pares,"00");
}


foreach ($pares as $par) {
	if ($par == "."){
		$raiz_aprox.=$par;
	}else{
		// Agregar par de números al resto
		$resto .= $par;
		// Buscar el siguiente dígito de la raíz
		$digito = 0;
		for ($i = 1; $i < 10; $i++) {
			if ((int)($auxiliar . $i) * $i <= (int)$resto) {
				$digito = $i;
			} else {
				break;
			}
		}    
		// Agregar digito a la raiz
		$raiz_aprox .= (string)$digito;    
		// Restar resultado al resto
		$resto = (string)((int)$resto - ((int)($auxiliar . $digito) * $digito));    
		// Obtener nuevo auxiliar
		$auxiliar = (string)((int)str_replace('.', '', $raiz_aprox) * 2);
	}
}

echo "Raiz: " . (float)$raiz_aprox;
?>
