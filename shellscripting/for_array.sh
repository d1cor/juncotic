#!/usr/bin/bash

nombres=(Diego Hugo Gonzalo Maria Daniel)

echo ${nombres[@]}
echo ${!nombres[@]}
echo ${#nombres[@]}

for nom in ${nombres[*]}; do
	echo "El nombre es: $nom"
done
echo "-------"
for i in ${!nombres[*]}; do
	echo "El nombre es: ${nombres[$i]}"
done

echo "-------"
for i in $(seq ${#nombres[*]}); do # 1 2 3 4 5
	echo "El nombre es: ${nombres[$(($i-1 ))]}"
done

echo "-------"
for i in $( seq 0 $((${#nombres[*]}-1 )) ); do # 0 1 2 3 4
	echo "El nombre es: ${nombres[ $i ]}"
done






