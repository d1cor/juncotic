import matplotlib.pyplot as plt

fig, ax = plt.subplots(1,2, figsize=(10, 5))
fig.tight_layout(pad=5.0)

valores = [25,12,75,43,12]
labels = ["Opción A","Opción B","Opción C","Opción D","Opción E"]
ax[0].pie(valores, labels=labels)
ax[0].set_title("Resultados encuesta")

x = [5,10,3,5,8,9,7,3,1,2,6,2,3,8,1]
y = [10,15,63,78,13,45,12,76,7,2,3,5,35,99,76]
ax[1].scatter(x, y, c="g")
ax[1].set_title("Título")
ax[1].set_xlabel("Eje X")
ax[1].set_ylabel("Eje Y")

plt.show()
