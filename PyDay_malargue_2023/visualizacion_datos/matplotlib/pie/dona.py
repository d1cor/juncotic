import matplotlib.pyplot as plt

valores = [25,12,75,43,12]
labels = ["Opción A","Opción B","Opción C","Opción D","Opción E"]
fig, ax = plt.subplots()
plt.pie(valores, labels=labels, wedgeprops={'width': 0.3})
plt.title("Resultados encuesta")
plt.show()
