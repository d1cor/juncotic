import folium
from folium.plugins import HeatMap
import pandas as pd

df = pd.read_csv('museos.csv')
mapa = folium.Map(location=[-34.6134945536014, -68.32745300275877], zoom_start=6)
latitud = df.Latitud.tolist()
HeatMap(list(zip(latitud, longitud))).add_to(mapa)
mapa.save("mapa_calor.html")
