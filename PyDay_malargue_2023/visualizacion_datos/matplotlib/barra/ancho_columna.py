import matplotlib.pyplot as plt

lugar = ["Ciudad A","Ciudad B","Ciudad C","Ciudad D"]
inscriptos = [10501, 11600, 9800, 12006]
fig, ax = plt.subplots()
plt.bar(lugar, inscriptos, 0.8 ,color = "g")
plt.title("Convocatoria Conferencia A")
plt.xlabel("Ciudad")
plt.ylabel("Inscriptos")
plt.show()
