import matplotlib.pyplot as plt

lugar = ["Ciudad A","Ciudad B","Ciudad C","Ciudad D"]
insc_A = [10501, 11600, 9800, 12006]
insc_B = [8023, 11500, 9100, 14006]
fig, ax = plt.subplots()
plt.bar(lugar, insc_A)
plt.bar(lugar, insc_B, bottom=insc_A)
plt.title("Convocatoria")
plt.xlabel("Ciudad")
plt.ylabel("Inscriptos")
plt.legend(["Conferencia A", "Conferencia B"])
plt.show()
