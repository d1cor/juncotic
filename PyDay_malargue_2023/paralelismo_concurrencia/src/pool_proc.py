import multiprocessing as mp

def cubo(n):
    #esto lo hace algún hijo
    return n**3

pool = mp.Pool(processes=2)

#ejemplo 1
resultado = pool.apply(cubo, args=(5,))

#ejemplo 2
resultado = pool.map(cubo, [3,4])



