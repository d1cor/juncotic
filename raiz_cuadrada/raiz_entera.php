<?php
// Colocar radicando como cadena de caracteres
$radicando = '25921';
$raiz_aprox = '0';
$resto = '';
$auxiliar = '';

// Obtener pares de números del radicando
if (strlen($radicando) % 2 == 0) {
    $pares = str_split($radicando, 2);
} else {
    $pares = array_merge(array($radicando[0]), str_split(substr($radicando, 1), 2));
}

foreach ($pares as $par) {
    // Agregar par de números al resto
    $resto .= $par;    
    // Buscar el siguiente dígito de la raíz
    $digito = 0;
    for ($i = 1; $i < 10; $i++) {
        if ((int)($auxiliar . $i) * $i <= (int)$resto) {
            $digito = $i;
        } else {
            break;
        }
    }    
    // Agregar digito a la raiz
    $raiz_aprox .= (string)$digito;    
    // Restar resultado al resto
    $resto = (string)((int)$resto - ((int)($auxiliar . $digito) * $digito));    
    // Obtener nuevo auxiliar
    $auxiliar = (string)((int)$raiz_aprox * 2);
}

echo "Raiz: " . (int)$raiz_aprox;
?>
