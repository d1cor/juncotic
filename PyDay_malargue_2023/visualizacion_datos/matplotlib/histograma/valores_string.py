import matplotlib.pyplot as plt

opiniones = ["Bueno","Muy bueno","Malo","Bueno","Bueno","Bueno","Bueno","Malo",
            "Muy bueno","Muy bueno","Bueno","Bueno",]
fig, ax = plt.subplots()
plt.hist(opiniones)
plt.title("Valoración Conferencia A")
plt.xlabel("Opinión")
plt.ylabel("Cantidad")
plt.show()
