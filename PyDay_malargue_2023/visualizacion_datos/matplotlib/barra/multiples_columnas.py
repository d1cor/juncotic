import matplotlib.pyplot as plt

lugar = ["Ciudad A","Ciudad B","Ciudad C","Ciudad D"]
insc_A = [10501, 11600, 9800, 12006]
insc_B = [8023, 11500, 9100, 14006]
ancho = 0.4
x1 = [0,1,2,3]
x2 = [x + ancho for x in x1]
fig, ax = plt.subplots()
plt.bar(x1, insc_A, ancho ,color = "g")
plt.bar(x2, insc_B, ancho)
plt.title("Convocatoria Conferencia A")
plt.xlabel("Ciudad")
plt.ylabel("Inscriptos")
plt.show()












"""
anchos = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
lugar = ["Ciudad A","Ciudad B","Ciudad C","Ciudad D"]
inscriptos = [10501, 11600, 9800, 12006]

for ancho in anchos:
    fig, ax = plt.subplots()
    plt.bar(lugar, inscriptos, ancho ,color = "g")
    plt.title("Convocatoria Conferencia A")
    plt.xlabel("Ciudad")
    plt.ylabel("Inscriptos")
    plt.show()
"""
