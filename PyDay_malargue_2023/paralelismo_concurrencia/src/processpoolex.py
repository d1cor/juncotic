from concurrent.futures \
        import ProcessPoolExecutor

def cubo(n):
    return n**3

pool = ProcessPoolExecutor(4)

future = pool.submit(cubo,(5))

print(future.result())


