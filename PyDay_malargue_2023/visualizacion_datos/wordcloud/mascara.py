from wordcloud import WordCloud
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

mask = np.array(Image.open("corazon.png"))
texto=open ("martinfierro.txt").read()
nube = WordCloud(stopwords=STOP_WORDS,
                 max_words = 100,
                 mask = mask
                ).generate(texto)
plt.imshow(nube,interpolation='bilinear')
plt.axis("off")
plt.show()







from wordcloud import WordCloud
import matplotlib.pyplot as plt
import nltk
from nltk.corpus import stopwords
import numpy as np
from PIL import Image
mask = np.array(Image.open("corazon.png"))
stop_words_es = stopwords.words("spanish")
stop_words = stop_words_es +stopwords.words("english")
stop_words = stop_words + ["Project","Gutenberg","work","electronic","tm","Foundation","works","donation","License","agreement","terms","state","eBook","full","States","copyright","paragraph","may","use","Archive","org","copy","literay"]
texto=open ("martinfierro.txt").read()
nube = WordCloud(stopwords=stop_words,
                 max_words = 100,
                 mask = mask,
                 #contour_width=3, contour_color='steelblue'
                ).generate(texto)
plt.imshow(nube,interpolation='bilinear')
plt.axis("off")
plt.show()
