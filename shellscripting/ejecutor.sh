#!/usr/bin/bash

# ./ejecutor.sh comando argumentos

SALIDA="/tmp/output"
ERRORES="/tmp/errors"

COMANDO=$*

$($COMANDO > $SALIDA 2> $ERRORES)
