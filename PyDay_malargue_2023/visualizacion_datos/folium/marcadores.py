import folium
import pandas as pd

df = pd.read_csv('museos.csv')
mapa = folium.Map(location=[-34.6134945536014, -68.32745300275877], zoom_start=5)
for indice, row in df.iterrows():
    folium.Marker(
        location=[row["Latitud"], row["Longitud"]],
        popup=row['nombre'],
        icon=folium.map.Icon(icon="home")
    ).add_to(mapa)
mapa.save("mapa_museos.html")
