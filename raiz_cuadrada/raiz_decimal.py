from math import ceil

# Convertir radicando a cadena de caracteres
radicando = str(120)
raiz_aprox = '0'
resto = ''
auxiliar = ''
cant_decimales = 3

#Obtener pares de numeros del radicando
if len(radicando) % 2 == 0:
	pares = [radicando[i:i+2] for i in range(0, len(radicando), 2)]
else:
	pares = [radicando[0]] + [radicando[i:i+2] for i in range(1, len(radicando), 2)]
	
# Agregar punto decimal
pares.append(".")

# Agregar tantos pares de ceros como decimales deseados
for i in range(cant_decimales):
	pares.append("00")
        
for par in pares:
	print(par)
	if par == ".":
		raiz_aprox+=par
	else:
		#Agregar par de números al resto
		resto += par	
		#Buscar el siguiente dígito de la raíz
		digito = 0
		for i in range(1,10):
			if int(auxiliar+str(i))*i <= int(resto):
				digito = i
			else:
				break
		#Agregar digito a la raiz
		raiz_aprox+=str(digito)
		#Restar resultado al resto
		resto = str(int(resto) - (int(auxiliar+str(digito))*int(digito)))
		#Obtener nuevo auxiliar
		auxiliar = str(int(raiz_aprox.replace('.', ''))*2)
print("Raiz: %f.cant_decimales" % float(raiz_aprox))	
