from wordcloud import WordCloud
import matplotlib.pyplot as plt

texto=("Regular Bueno Regular Excelente Malo Bueno Bueno Bueno Bueno Malo Regular Regular Bueno Bueno")
nube = WordCloud().generate(texto)
plt.imshow(nube,interpolation='bilinear')
plt.axis("off")
plt.show()
