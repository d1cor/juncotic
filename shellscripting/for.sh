#!/usr/bin/bash

for var in 1 a 3 b 5 c ; do
	# sentencias o comandos
	echo "Valor de la variable var: $var"
done

for i in $(seq 10); do    # 1 2 3 4 5 6 7 8 9 10
	echo "El valor de i: $i"
done

for i in $(seq 5 -1 1); do  # 5 4 3 2 1
	echo "Cuenta regresiva: $i"
	sleep 1
done
echo "BOOOOOOOMMMM"


echo "Finalizando el script."
