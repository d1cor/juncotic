#!/usr/bin/bash

function funcionVars {
	echo "Ejecucion de la funcion"
	echo "Valor en la funcion, antes de modificarla: $VAR"
	local VAR="Mundo"
	echo "Valor dentro de la funcion: $VAR"
}

function funcionVars2 {
	local VAR="que tal"
	echo "valor en la segunda funcion: $VAR"
}

# script principal
#variable global
VAR="Hola"
echo "Variable en el script vale: $VAR"

funcionVars
funcionVars2

echo "Variable en el script vale (luego de invocar a la funcion): $VAR"





