import matplotlib.pyplot as plt

edades = [23,22,28,32,24,28,32,15,26,22,24,24,26,28,32,41,20,39,51,18,23,28,26,34,17]
fig, ax = plt.subplots()
plt.hist(edades)
plt.title("Edades Conferencia A")
plt.xlabel("Edades")
plt.ylabel("Cantidad")
plt.show()
