#!/usr/bin/bash

# ejemplo break
for ((i=0; i<10; i++)); do
	if [ $i -eq 6 ]; then
		# sale del bucle
		break
	fi

	echo "Valor de i: $i"
done

echo "--------------------"
# ejemplo continue
for ((i=0; i<10; i++)); do
	if [ $i -eq 6 ]; then
		# sale del bucle
		continue
	fi

	echo "Valor de i: $i"
done

