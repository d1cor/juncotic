import matplotlib.pyplot as plt

valores = [25,12,75,43,12]
labels = ["Opción A","Opción B","Opción C","Opción D","Opción E"]
explode = [0.05, 0.05, 0.05, 0.05, 0.05]
fig, ax = plt.subplots()
plt.pie(valores, labels=labels, explode = explode)
plt.title("Resultados encuesta")
plt.show()
