import plotly.express as px
import pandas as pd

df = pd.read_csv('cinear-total-horas-vistas.csv')
fig = px.scatter(df, x="indice_tiempo", y="total_horas_vistas",
	         size="total_horas_vistas", color="total_horas_vistas",
                 hover_name="total_horas_vistas", size_max=60)
fig.show()
