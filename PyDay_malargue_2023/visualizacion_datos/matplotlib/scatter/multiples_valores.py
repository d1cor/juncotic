import matplotlib.pyplot as plt

x1 = [5,10,3,5,8,9,7,3,1,2,6,2,3,8,1]
y1 = [10,15,63,78,13,45,12,76,7,2,3,5,35,99,76]

x2 = [5,1,8,3,6,3,6,3,2,6,7,1,5,7,2]
y2 = [16,65,6,72,53,46,16,72,51,22,35,1,5,9,16]

fig, ax = plt.subplots()
plt.scatter(x1, y1, c="g")
plt.scatter(x2, y2, c="r")
plt.title("Título")
plt.xlabel("Eje X")
plt.ylabel("Eje Y")
plt.show()
