import matplotlib.pyplot as plt

anio = [2018,2019,2020,2021,2022,2023,2024]
inscriptos = [10501, 11600, 9800, 12006, 12989, 13085, 15138]
fig, ax = plt.subplots()
plt.plot(anio, inscriptos)
plt.show()
