#!/usr/bin/bash
# Ejecucion: sh for_archivos.sh /home

DIR=$1

for elemento in $(ls $DIR); do
	RUTAABSOLUTA="$DIR/$elemento"
	echo "Elemento: $RUTAABSOLUTA"

	if [ -f $RUTAABSOLUTA ]; then
		echo "$RUTAABSOLUTA es un archivo."
	elif [ -d $RUTAABSOLUTA ]; then
		echo "$RUTAABSOLUTA es un directorio."
	else
		echo "$RUTAABSOLUTA es de un tipo desconocido."
	fi

done

