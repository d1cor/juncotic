import plotly.express as px
import pandas as pd

df = pd.read_csv('ar.csv')
fig = px.treemap(df, path=['pais', 'provincia', 'ciudad'],
                 values = 'poblacion',
                 color ="poblacion")
fig.update_traces(root_color="lightgrey")
fig.update_layout(margin = dict(t=50, l=25, r=25, b=25))
fig.write_image("treemap.png")
