import asyncio

async def cubo(n):
    await asyncio.sleep(1)
    return n**3

async def main():
    r1 = cubo(5)
    r2 = cubo(6)
    r3 = cubo(7)

    res = await asyncio.gather(
            r1, r2, r3)
    return res

resultado = asyncio.run(main())
print(resultado)
