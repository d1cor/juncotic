import matplotlib.pyplot as plt

x = [5,10,3,5,8,9,7,3,1,2,6,2,3,8,1]
y = [10,15,63,78,13,45,12,76,7,2,3,5,35,99,76]
fig, ax = plt.subplots()
plt.scatter(x, y, c="g")
plt.title("Título")
plt.xlabel("Eje X")
plt.ylabel("Eje Y")
plt.show()
