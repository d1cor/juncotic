import multiprocessing as mp

def cubo(n):
    # esto lo hce el hijo
    print(n**3)
    exit(0)

# esto lo hace el padre
hijo = mp.Process(
        target=cubo, args=(5,))

hijo.start()
hijo.join()



