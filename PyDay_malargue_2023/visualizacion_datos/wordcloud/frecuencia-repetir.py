from wordcloud import WordCloud
import matplotlib.pyplot as plt

texto = {'Python': 50, 'C++': 10, 'C': 5, 'PHP': 8, 'C#': 15}
nube = WordCloud(background_color = "white",
                 colormap = "cividis",
                 repeat = True
                ).generate_from_frequencies(texto)
plt.imshow(nube,interpolation='bilinear')
plt.axis("off")
plt.show()
