#!/usr/bin/bash

BASEDIR="/tmp/"

read -p "Qué desea crear? (file o dir): " TIPO
read -p "Nombre del elemento: " NOMBRE

case $TIPO in
	'file')
		touch $BASEDIR$NOMBRE
		;;
	'dir')
		mkdir $BASEDIR$NOMBRE
		;;
	*)
		echo "Operación incorrecta."
esac
