import matplotlib.pyplot as plt

lugar = ["Ciudad A","Ciudad B","Ciudad C","Ciudad D","Ciudad E","Ciudad F","Ciudad G"]
inscriptos = [10501, 11600, 9800, 12006, 12989, 13085, 15138]
fig, ax = plt.subplots()
plt.plot(lugar, inscriptos, "o--")
plt.title("Convocatoria Conferencia A")
plt.xlabel("Ciudad")
plt.ylabel("Inscriptos")
plt.savefig("nombre-archivo.png")
