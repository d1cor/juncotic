import matplotlib.pyplot as plt

lugar = ["Ciudad A","Ciudad B","Ciudad C","Ciudad D","Ciudad E","Ciudad F","Ciudad G"]
insc_A = [10501, 11600, 9800, 12006, 12989, 13085, 15138]
insc_B = [8023, 11500, 9100, 14006, 14989, 10085, 13138]
fig, ax = plt.subplots()
plt.plot(lugar, insc_A, "o--m")
plt.plot(lugar, insc_B, "^:g")
plt.title("Convocatoria")
plt.xlabel("Ciudad")
plt.ylabel("Inscriptos")
plt.grid(True)
plt.legend(["Conferencia A", "Conferencia B"])
plt.show()
