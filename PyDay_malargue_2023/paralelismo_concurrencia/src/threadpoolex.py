from concurrent.futures \
        import ThreadPoolExecutor

def cubo(n):
    return n**3

pool = ThreadPoolExecutor(4)

future = pool.submit(cubo,(5))

print(future.result())


