import matplotlib.pyplot as plt

opinionesA = ["Muy bueno","Bueno","Muy bueno","Malo","Bueno","Bueno","Bueno","Bueno","Malo",
             "Muy bueno","Muy bueno","Bueno","Bueno"]
opinionesB = ["Malo","Bueno","Malo","Bueno","Malo","Malo","Bueno","Malo","Bueno",
             "Muy bueno","Bueno","Bueno",]
fig, ax = plt.subplots()
plt.hist([opinionesA,opinionesB],stacked=True)
plt.title("Valoración Conferencias")
plt.xlabel("Opinión")
plt.ylabel("Cantidad")
plt.legend(["Conferencia A","Conferencia B"])
plt.show()
