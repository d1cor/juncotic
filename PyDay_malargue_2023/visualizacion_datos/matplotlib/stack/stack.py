import matplotlib.pyplot as plt

x = [1,2,3,4,5,6,7,8]
x1 = [5,10,3,5,8,9,7,3]
x2 = [6,7,3,1,7,3,2,5]
x3 = [2,1,9,8,4,3,2,2]
x4 = [1,7,3,2,7,4,3,4]
colors= ['m','c','r','b']
fig, ax = plt.subplots()
plt.stackplot(x,x1,x2,x3,x4, colors = colors)
plt.title("Título")
plt.xlabel("Eje X")
plt.ylabel("Eje Y")
plt.show()
