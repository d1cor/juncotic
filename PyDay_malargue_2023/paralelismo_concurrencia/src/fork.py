import os

ret = os.fork()

if ret==0:
    #cosas del hijo
    print(5**3)
    exit(0)
else:
    #cosas del padre
    os.wait()
