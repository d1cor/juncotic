#!/usr/bin/bash

funcion() {
	echo "ejecucion de comando 1"
	echo "ejecucion de comando 2"
	echo "ejecucion de comando 3 modificado"
	echo "ejecucion de comando 4"
	echo "ejecucion de comando 5"
}

function otraFuncion {
	echo "comandos de la otra funcion"
}

echo "script principal"
funcion
otraFuncion
echo "saliendo del script"
