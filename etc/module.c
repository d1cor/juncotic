/**************************************************************
 * Modulo vs Resto en C
 *
 * En lenguaje C el operador % calcula el resto de la división, no el módulo de una división Euclidiana.
 *
 * Para valores positivos ambas coinciden, pero para valores negativos hay diferencias.
 *
 * Ejemplos de resto de división euclidiana (operador % de C):
 *
 * División entera:
 * 37 / 5 = 7 porque 7*5 = 35, al 37 hay 2
 * Resto: 2
 *
 * -37 / 5 = -7 porque -7*5 = 35, al -37 hay -2
 * Resto: -2
 *
 *
 * Ejemplos de módulo:
 * 37 / 5 = 7 porque 7*5 = 35, al 37 hay 2
 * Modulo: 2
 *
 *
 * -37 / 5 = -8 porque -8*5 = -40, al -37 hay 3
 * Modulo: 3
 *
 *
*/

#include<stdio.h>
#include<math.h>
#define MOD(x,y) ((((x)%(y))+(y))%(y))

int mod(int x, float y){  
	return x - floor(x/y)*y;
}

int main(){
	int b, x=-37, y=5;
	b = x%y;
	printf("%d\n", b);
	printf("%d\n", mod(x,y));
	printf("%d\n", MOD(x,y));
	return 0;
}



